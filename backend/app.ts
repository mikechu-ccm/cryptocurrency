import * as express from 'express';
import * as dotenv from 'dotenv';
import * as cors from 'cors';
import * as redis from "redis";
import * as http from 'http';
import * as socketIO from 'socket.io';
import fetch from 'node-fetch';

//express setting
dotenv.config();
const app = express();
const PORT = 8000;

//socket.io setting
const server = new http.Server(app);
const io = socketIO(server);

//redis setting
const client = redis.createClient();
client.on("error", function (err) {
    console.log("Error " + err);
});

function redisSet(data:any){
    client.set("price", JSON.stringify(data), 'EX', 30, redis.print);
}

//cors
app.use(cors())



export const fetchCurrency = async (currencyName) => {
    const result = await Promise.all(currencyName.map(async (name) => {
        try {
            const json = await fetch(`https://api.cryptonator.com/api/ticker/${name}-usd`, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json"
                }
            });
            const result = await json.json();
            return result["ticker"]
        } catch (e) {
            console.log('Catched error', e)
        }
        })
    )
    return result
}

//Call API to fetch data
let autoRun=false;
export const getAll = async () => {
    console.log("getAll fire")
    const currencyName = ['btc', 'ltc', 'xmr', 'xrp', 'doge', 'dash', 'maid', 'lsk']
    const currencyDetails = await fetchCurrency(currencyName)

    //Redis - insert data
    redisSet(currencyDetails)
    io.sockets.emit('getMessage', currencyDetails)
    return currencyDetails
}


//socket.io emit result
let intervalID=null;
let onlineUserNumber=0;

io.on('connection', function (socket) {
    console.log("connected")
    onlineUserNumber++
  
    //counting user number. If no user, stop fetching.
    socket.on("disconnect",()=>{
        onlineUserNumber--
        if(onlineUserNumber===0){
            clearInterval(intervalID)
            autoRun=false
            console.log("setInterval has stopped")
        }
    })

    if(onlineUserNumber>0 && autoRun){
        client.get("price", async function (err, reply) {
            socket.emit('getMessage', JSON.parse(reply))
        })
    }else if(onlineUserNumber>0 && !autoRun){
        client.get("price", async function (err, reply) {

            if (reply!==null){
                socket.emit('getMessage', JSON.parse(reply))
            }else{
                socket.emit('getMessage',await getAll())
            }
            intervalID=setInterval(()=>{getAll();autoRun=true}, 30000)
        })
    }
})

server.listen(PORT, () => {
    console.log(`link: http://localhost:${PORT}`);
})

