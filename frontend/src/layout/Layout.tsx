import React from 'react';
import CurrencyBox from '../components/currencyBox/CurrencyBox'
import { CurrencyFormat } from '../interface';
import io from 'socket.io-client';

const { REACT_APP_API_SERVER } = process.env

interface State {
    details:[CurrencyFormat]|null;
    socket:SocketIOClient.Socket;
}

class Layout extends React.Component<{},State>{
    
    constructor(props: {}) {
        super(props);
        this.state = {
            details: null,
            socket:io(`${REACT_APP_API_SERVER}`)
        }
    }

    componentDidMount = async () => {
        console.log(REACT_APP_API_SERVER)

        this.state.socket.on('getMessage', (message:any) => {
            console.log(message)
            this.setState({
                details:message
            })
        })
    }


    render(){
        return(
            <div className="container" >
                <h1>Cryptocurrency Realtime Price</h1>
                <div className="box-wrapper">
                    {this.state.details===null? "Loading...":
                        <>
                        {this.state.details.map((info,index) => {         
                            return (
                                <div key={index} className="box" >
                                    <CurrencyBox currency={info} />
                                </div>
                            )
                        })}
                        </>
                    }
                </div>
            </div>
    )}
}

// class Layout extends React.Component<{}, State> {

//     constructor(props: {}) {
//         super(props);
//         this.state = {
//             details: null,
//             socket:io(`${REACT_APP_API_SERVER}`)
//         }
//     }

//     componentDidMount = async () => {
//         console.log(REACT_APP_API_SERVER)

//         this.state.socket.on('getMessage', (message:any) => {
//             console.log(message)
//             this.setState({
//                 details:message
//             })
//         })
//     }



//     render() {
//         console.log(this.state.details)
//         return (
//             <div className="container" >
//                 <h1>Cryptocurrency Realtime Price</h1>
//                 <div className="box-wrapper">
//                     {this.state.details===null? "Loading...":
//                         <>
//                         {this.state.details.map((info,index) => {         
//                             return (
//                                 <div key={index} className="box" >
//                                     <CurrencyBox currency={info} />
//                                 </div>
//                             )
//                         })}
//                         </>
//                     }
//                 </div>
//             </div>
//         );
//     }
// }

export default Layout
