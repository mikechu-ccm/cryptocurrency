export interface CurrencyFormat{
    base: string,
    target: string,
    price: number,
    volume: number,
    change: number
}