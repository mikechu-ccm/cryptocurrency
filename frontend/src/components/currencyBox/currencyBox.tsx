import React from 'react';
import { CurrencyFormat } from '../../interface';

interface Props {
  currency: CurrencyFormat
}

const currencyBox: React.FC<Props> = (props) => {
  const name: any = {
    "BTC": "Bitcoin",
    "ETH": "Ether",
    "LTC": "Litecoin",
    "XMR": "Monero",
    "XRP": "Ripple",
    "DOGE": "Dogecoin",
    "DASH": "Dash",
    "MAID": "MaidSafeeCoin",
    "LSK": "Lisk",
    "SJCX": "Storjcoin X"
  }

  return (
    <>
      <div className="currency-name">{name[props.currency.base]}</div>
      <div className="currency-price">${props.currency.price}</div>
      <div className="currency-flex">
        <div className="currency-volume"><p>volume:</p>{props.currency.volume || "-"}</div>
        <div className="currency-change"><p>change:</p><span className={props.currency.change >= 0 ? "pos-num" : "neg-num"}>{props.currency.change}</span></div>
      </div>
    </>
  )
}

export default currencyBox;


